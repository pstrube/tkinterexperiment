#!/usr/bin/python
# this project exists so I can learn the ins and outs of GUI programming with Python

# this project was created using this tutorial: http://sebsauvage.net/python/gui/

import Tkinter


class TheApp(Tkinter.Tk):

    def __init__(self, parent):
        Tkinter.Tk.__init__(self,parent)
        self.parent = parent
        self.initialize()

    def initialize(self):
        # code all form widgets here
        self.grid()  # create grid layout manager

        self.entryVariable = Tkinter.StringVar()
        self.entry = Tkinter.Entry(self,textvariable=self.entryVariable)  # textbox
        self.entry.grid(column=0, row=0, sticky='EW')  # widget sticks to East and West window walls
        self.entry.bind("<Return>", self.on_press_enter)  # enter keybinding to listener
        self.entryVariable.set(u"Enter text here")

        button = Tkinter.Button(self, text=u"Click me!",
                                command=self.on_button_click)  # button, button event listener binding
        button.grid(column=1, row=0)  # button position

        self.labelVariable = Tkinter.StringVar()
        # ------------------------> left aligned text, white foreground, blue background
        label = Tkinter.Label(self, textvariable=self.labelVariable,
                              anchor="w", fg="white", bg="blue")  # label
        label.grid(column=0, row=1, columnspan=2, sticky='EW')  # positioning
        self.labelVariable.set(u"Hello!")

        self.grid_columnconfigure(0, weight=1)  # resizes first column on window resize
        self.resizable(True, False)  # window is only resizeable horizontally
        self.update()
        self.geometry(self.geometry())  # fixes the size of the window so long strings don't grow it
        self.entry.focus_set()  # focuses textbox on initialization
        self.entry.select_range(0, Tkinter.END)

    def on_button_click(self):  # button click event handler, gets textbox text, displays
        self.labelVariable.set(self.entryVariable.get() + "   (You clicked the button)")  # changes label text
        self.entry.focus_set()  # focuses textbox on click
        self.entry.select_range(0, Tkinter.END)

    def on_press_enter(self,event):  # enter key event handler, gets textbox text, displays
        self.labelVariable.set(self.entryVariable.get() + "   (You pressed ENTER)")  # changes label text
        self.entry.focus_set()  # focuses textbox on enter press
        self.entry.select_range(0, Tkinter.END)

if __name__ == "__main__":
    app = TheApp(None)  # create app instance
    app.title('TITLEASS')  # sets title bar
    app.mainloop()  # sets app to loop indefinitely and listen for events
